

import os , sys , shutil
from pathlib import Path
from PIL import Image


class ImageProcessing(object):
	def __init__(self , path):
		self.path = path
		self.condition = False
		self.seperator = '\n'

	def path_validation(self):
		''' Check the path if it is correct '''
		return False if not Path(self.path).is_dir() else self.path

	def sort_photos(self,path_image):
		''' Sort all photos and Copy Photos Another Folder '''
		try :
			self.list_image = list(filter(lambda x:x.endswith(('.png','.jpg','.jpeg')),os.listdir(path_image)))
			return self.list_image

		except Exception as error :
			return (f'Error In Function (sort photo)!')

	def view_image_resolution(self , path_img , list_image):
		''' view_image_resolution '''
		self.dict_pixels = dict()

		try:
			for (self.target_image) in self.sort_photos(path_img):
				image = Image.open(path_img + '\\' + self.target_image)

				self.width , self.height = image.size
				self.dict_pixels[(self.target_image)] = sum([self.width , self.height])

		except FileNotFoundError as error :
			return (f'Error In Function File Not Found Error (view image resolution)!')
		except Exception as error :
			return (f'Error In Function (view image resolution)!')

		return self.dict_pixels


	def return_path_image(self):
		''' Return Image Path '''
		if self.path_validation() == False :
			return (f'Invalid The Path please try again!')
		return self.path_validation()

	def show_best_picture(self,image):
		''' Show the best picture '''

		if self.path_validation() == False :
			return (f'Error')

		self.name , self.resulation = '',0
		for(self.name_image , self.resulation_image) in self.dict_pixels.items():
			if (self.resulation < self.resulation_image):
				self.name , self.resulation = (self.name_image , self.resulation_image)

		if (self.resulation <= 0):
			return False

		self.best_image = Image.open(self.path_validation() + '\\' + self.name)
		self.best_image.show()
		


if __name__ == '__main__':
	main_object = ImageProcessing(r'C:\Users\alobe\OneDrive\Pictures')
	res1 = main_object.path_validation()
	res2 = main_object.sort_photos(res1)

	res3 = main_object.view_image_resolution(res1,res2)
	print(main_object.show_best_picture(res3))
